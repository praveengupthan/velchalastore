<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/headerPostlogin.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">  
     <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Praveen Guptha N</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                         <li class="breadcrumb-item"><a href="userProfile.php">My Profile</a></li>
                         <li class="breadcrumb-item active" aria-current="page"><span>Change Password</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody">    
            <!-- container -->       
           <div class="container">

            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                <div class="col-md-4 col-sm-4">
                    <?php include 'includes/usrleftnav.php' ?>
                </div>
                <!--/ left col -->
                <!-- right col -->
                <div class="col-md-8 col-sm-8">

                <!-- right profile detail -->
                    <div class="user-profile-rt">   
                        
                     <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-6">
                                <form class="form">
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="currentPassword">Current Password</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="currentPassword" aria-describedby="emailHelp" placeholder="Enter Current Password">          
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="newPassword">New Password</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="newPassword" aria-describedby="emailHelp" placeholder="Enter New Password">          
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    
                                    <!-- form group -->
                                    <div class="form-group">
                                        <label for="confirmNewPassword">Confirm New Password</label>
                                        <div class="input-group">
                                            <input type="password" class="form-control" id="confirmNewPassword" aria-describedby="emailHelp" placeholder="Confirm New Password">          
                                        </div>
                                    </div>
                                    <!--/ form group -->

                                    <div class="form-group">
                                        <input type="button" value="SAVE PASSWORD" class="orange-btn">
                                    </div>
                                </form>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->


                      

                     

                    </div>
                    <!--/ right profile detail -->
                   
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
                
            </div>
            <!--/ container -->                
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>     
    
</body>
</html>