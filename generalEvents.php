<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Events</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page"><span>Events</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
           <div class="container">                
            <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <?php
                        for($i=0;$i<count($blogEventItem);$i++) {?>
                        <div class="col-md-6 col-lg-4">
                            <div class="card blogcard">
                                <a href="generalEventDetail.php">
                                    <img class="card-img-top img-fluid" src="img/albums/<?php echo $blogEventItem[$i][0]?>.jpg">
                                </a>
                                <div class="card-body">
                                    <h5 class="card-title ptregular"><?php echo $blogEventItem[$i][1]?></h5>
                                  
                                    <p><?php echo $blogEventItem[$i][3]?> <span class="d-inline-block px-3 small pb-3">|</span><?php echo $blogEventItem[$i][4]?></p>
                                    <a href="generalEventDetail.php" class="btn orange-btn">Read More</a>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <!--/ col -->                  
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row pb-5">
                    <div class="col-md-12">
                        <nav>
                            <ul class="pagination justify-content-center">                                        
                                <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                                    <span class="page-link" aria-hidden="true">‹</span>
                                </li>      
                                <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)" rel="next" aria-label="Next »">›</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--/ row -->

            </div>
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>
</html>