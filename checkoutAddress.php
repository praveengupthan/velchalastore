<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Deliver address</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Deliver address</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody">
           
            <!--container -->
            <div class="container">
                <!-- row -->
                <div class="row py-3">
                    <!-- left col -->
                    <div class="col-lg-8">

                    <h3 class="h5 fbold fblue">Select Delivery Address</h3>

                     <!-- address item -->
                    <div class="address-item">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-1 col-2 pr-0">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>    
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-8 col-10 pl-0">
                                <p class="fsbold">Praveen Guptha Nandipati</p>
                                <p>Plot No:91, 4-32-51/51/1, Kamala Prasanna Nagar, Kukatpally, Hyderabad, Telangana, </p>
                                <a href="javascript:void(0)" class="btn-deliver blue-btn">Deliver Here</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-3 col-12 text-sm-center text-right">
                                <a href="javascript:void(0)" class="forange editicon">Edit</a>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                    <!--/ address item-->

                    
                    <!-- address item -->
                    <div class="address-item">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-1 col-2 pr-0">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1">    
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-8 col-10 pl-0">
                                <p class="fsbold">Praveen Kumar (Home Address)</p>
                                <p>Plot No:91, 4-32-51/51/1, Kamala Prasanna Nagar, Kukatpally, Hyderabad, Telangana, Allwyn Colony Phase 1, Road No:9, Hydeabad - 500072.</p>
                                <a href="javascript:void(0)" class="btn-deliver blue-btn">Deliver Here</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-3 col-12 text-sm-center text-right">
                                <a href="javascript:void(0)" class="forange editicon">Edit</a>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                    <!--/ address item-->

                    
                    <!-- address item -->
                    <div class="address-item">
                        <!-- row -->
                        <div class="row">
                            <!-- col -->
                            <div class="col-md-1 col-2 pr-0">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="option1" checked>    
                                </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-8 col-10 pl-0">
                                <p class="fsbold">Praveen Nandipati</p>
                                <p>Plot No:91, 4-32-51/51/1, Kamala Puri Nagar, Srinagar Colony, Hyderabad, Telangana, Hydeabad - 500072.</p>
                                <a href="javascript:void(0)" class="btn-deliver blue-btn">Deliver Here</a>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-3 col-12 text-sm-center text-right">
                                <a href="javascript:void(0)" class="forange editicon">Edit</a>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row --> 
                    </div>
                    <!--/ address item-->

                    <p class="text-center">
                        <a data-toggle="modal" data-target="#newAddress" class="fblue" href="javascript:void(0)">+ Add New Address</a>
                    </p>   

                    <p class="pt-3 text-right">
                        <a href="checkoutOrders.php" class="orange-btn text-center w-100">Confirm Address</a>
                    </p>

                    </div>
                    <!--/ left col -->

                     <!-- left col -->
                     <div class="col-lg-4 pt-2 pt-md-0">
                         <!-- card -->
                         <div class="card cartcard">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Order Summary</h4>
                             </div>
                             <div class="card-body">
                                 <p class="h6 d-flex justify-content-between  pb-4">
                                    <span>ITEMS 3</span>
                                    <span class="price-single">750</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between  pb-4 border-bottom">
                                    <span>Shipping Charges</span>
                                    <span class="price-single">150</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between fsbold py-4">
                                    <span>Total Cost</span>
                                    <span class="price-single fblue">900</span>
                                 </p>
                                
                             </div>
                         </div>
                         <!--/ card -->
                     </div>
                    <!--/ left col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

            
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>

    <!-- Add New Address -->
    <div class="modal fade" id="newAddress" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content ">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Add New Address</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">

        <!--form-->
        <form class="form">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="firstName">First Name</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="firstName" aria-describedby="firstName" placeholder="First Name">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="secondName">Second Name</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="secondName" aria-describedby="firstName" placeholder="Second Name">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="address01">Address Line 01</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="address01" aria-describedby="address01" placeholder="Address Line 01">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="address02">Address Line 02</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="address02" aria-describedby="address02" placeholder="Address Line 02">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="pinNumber">Pin Number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="pinNumber" aria-describedby="pinNumber" placeholder="Write Pin Number">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="landmark">Land Mark</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="landmark" aria-describedby="landmark" placeholder="Ex:Beside Hospital">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="city">City</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="city" aria-describedby="city" placeholder="Ex:Hyderabad">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="State">State</label>
                            <div class="input-group">
                                <select id="State" class="form-control">
                                    <option selected>Choose...</option>
                                    <option>Telangana</option>
                                    <option>Andhra Pradesh</option>
                                    <option>Tamil Nadu</option>
                                    <option>Karnataka</option>
                                    <option>Kerala</option>
                                    <option>Madhya Pradesh</option>
                                </select>
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                
                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="mNumber">Mobile Number</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="mNumber" aria-describedby="mNumber" placeholder="Mobile Number">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

                <!-- col -->
                <div class="col-lg-6">
                    <!-- form group -->
                    <div class="form-group">
                            <label for="email">Email</label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="email" aria-describedby="email" placeholder="Enter Valid Email Address">
                            </div>
                        </div>
                        <!-- /form group -->
                </div>
                <!--/ col -->

            </div>
            <!--/ row -->
        </form>
        <!--/ form -->
        
        </div>
        <div class="modal-footer">       
            <button type="button" class="orange-btn">Save Address</button>
        </div>
        </div>
    </div>
    
    </div>
    <!--/Add New Address -->
    
</body>
</html>