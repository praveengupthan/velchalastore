<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <!-- <h1>Telangana Tejam</h1> -->
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Telangana Tejam</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody  py-3">

        <!-- book detail -->
         <div class="book-top-detail ">
            <!-- container -->
            <div class="container">
                <!-- card -->
                <div class="card p-2 p-sm-2 p-lg-5">
                    <!-- alert add to cart-->
                         <div id="alertAddtocart" class="alert alert-success alert-dismissible" role="alert">
                            <strong><span class="icon-check"></span> Success!</strong> Your Product <strong>Telangana Tejam</strong> Added Successfully to your cartlist.  <a class="d-block" href="cart.php"><strong>View Now</strong></a>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <!-- row -->
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-5 col-md-4">
                            <figure class="figure-detail">
                                <img src="img/books/2.jpg" alt="" class="img-fluid">
                            </figure>
                        </div>
                        <!--/ col -->
                         <!-- col -->
                             <div class="col-lg-7 col-md-8 book-detail-rt align-self-center">
                                 <!-- border -->
                                <div class="px-4 py-3 border">
                                    <!-- row -->
                                    <div class="row position-relative">
                                        <div class="col-md-9 col-12">
                                            <h1 class="h2 fsbold">Telangana Tejam</h1>
                                            <p class="price pl-1">
                                                <span class="offer">350</span>
                                                <span class="oldprice">400</span>
                                            </p>
                                        </div>                                       
                                    </div>
                                    <!--/ row -->                                   
                                </div>
                                <!-- border/-->

                                <!-- border -->
                                <div class="px-4 border mt-2 wow animate__animated animate__fadeInDown">
                                    <!-- specifications -->
                                    <div class="book-specs">
                                     <!-- row -->
                                     <div class="row">
                                         <!--col-->
                                         <div class="col-sm-4 col-6 col-md-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Availability</dt>
                                                    <dd class="fgreen fsbold">In Stock</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 col-md-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>SKU Code</dt>
                                                    <dd>CA78963</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 col-md-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Pages</dt>
                                                    <dd>215</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 col-md-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Author</dt>
                                                    <dd>Dr. Velchala Kondal Rao</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 col-md-6 brd-rt brd-lt">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Language</dt>
                                                    <dd>Telugu</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 col-md-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Publisher</dt>
                                                    <dd>Sister Niveditha</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->
                                          <!--col-->
                                          <div class="col-sm-4 col-6 col-md-6">
                                            <div class="speccol">
                                                <dl>
                                                    <dt>Published Date</dt>
                                                    <dd>11-05-2012</dd>
                                                </dl>
                                            </div>
                                         </div>
                                         <!--/ col -->                                          
                                     </div>
                                     <!--/ row -->
                                 </div>
                                 <!--/ specifications -->
                                </div>
                                <!--/ border -->

                                <p class="py-3">
                                    <span>QTY</span>
                                    <span>
                                        <input class="text-center qty" type="number" placeholder="1">
                                    </span>
                                </p>

                                <a href="img/pdf-book.pdf" class="orange-btn" target="_blank">Read Online</a>
                                <a id="addtokcart-icon" href="javascript:void(0)" class="blue-btn">Add to Cart</a>

                                <!-- social share -->
                                <div class="social-share pt-4">
                                    <span class="pt-1 span-share">Share this product</span>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-instagram icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>                                   
                                </div>
                                <!--/ social share -->                                
                             </div>
                            <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ card -->

                <!-- responsive tab -->
                    <div class="custom-tab">
                        <!-- tab -->
                        <div class="parentHorizontalTab">
                            <ul class="resp-tabs-list hor_1 wow animate__animated animate__fadeInUp">
                                <li>Description</li>                              
                                <li>Book Contributors</li>
                            </ul>
                            <div class="resp-tabs-container hor_1 wow animate__animated animate__fadeInUp">
                                <!-- description -->
                                <div>
                                    <p>Lorem ipsum dolor sit amet, qui sumo copiosae ea, vim dolorum volumus ei. Mei ei dolor tamquam deleniti. Habemus copiosae his cu, quo ne reque augue. Nonumy contentiones his eu, vel falli copiosae inimicus te. Liber commune et vix. Putant quaeque inimicus cu usu, eum civibus delicatissimi an. Tota suavitate mnesarchum ad nec, at vim propriae voluptua. In vim laudem cetero interpretaris. Civibus vivendum conclusionemque mel eu. Homero dolorum phaedrum sed no, eos oratio forensibus te. Mea possim accusamus theophrastus ex, ea dicit patrioque sit. Vix possim volutpat suscipiantur cu, ex nam purto invidunt, ut qui nominati pertinacia.</p>
                                   
                                </div>
                                <!--/ description -->

                                <!-- Contributors -->
                                <div class="contributors">
                                 <p class="text-center">
                                    No Data Available Now
                                </p>

                                <table>
                                    <tr>
                                        <td>The living legend in Indian literature</td>
                                        <td>Seshendra Sharma</td>
                                        <td>21</td>
                                    </tr>
                                     <tr>
                                        <td>The living legend in Indian literature</td>
                                        <td>Seshendra Sharma</td>
                                        <td>21</td>
                                    </tr>
                                     <tr>
                                        <td>The living legend in Indian literature</td>
                                        <td>Seshendra Sharma</td>
                                        <td>21</td>
                                    </tr>
                                     <tr>
                                        <td>The living legend in Indian literature</td>
                                        <td>Seshendra Sharma</td>
                                        <td>21</td>
                                    </tr>
                                     <tr>
                                        <td>The living legend in Indian literature</td>
                                        <td>Seshendra Sharma</td>
                                        <td>21</td>
                                    </tr>
                                     <tr>
                                        <td>The living legend in Indian literature</td>
                                        <td>Seshendra Sharma</td>
                                        <td>21</td>
                                    </tr>
                                </table>                                   
                                </div>
                                <!--/ Contributors -->
                            </div>
                        </div>
                        <!--/ tab -->
                    </div>
                    <!--/ responsive tab -->

                    <!-- you may also interested in -->
                    <div class="similar-books wow animate__animated animate__fadeInUp">
                        <h2 class="h4 fbold border-bottom py-3">You may also be interested in</h2>
                        <!-- row-->
                <div class="row py-3">
                    <!-- col -->
                    <?php 
                    for ($i=0; $i<4;$i++) { ?>
                    <div class="col-6 col-sm-4 col-md-3 text-center">
                        <div class="book-item">
                             <a href="javascript:void(0)">
                                 <img src="img/books/<?php echo $publications[$i][0] ?>.jpg" alt="" class="img-fluid">
                             </a>
                            <article><a href="javascript:void(0)"><?php echo $publications[$i][1]?></a></article>
                        </div>
                    </div>
                    <?php } ?>
                    <!--/ col -->
                </div>
                <!-- row -->
                    </div>
                    <!--/ you may also interested in -->




            </div>
            <!--/ container -->
         </div>
         <!--/ book detail -->
           
           

            
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>

    <script>
    $(document).ready(function(){ 

        $('#addtokcart-icon').click(function(){
            $('#alertAddtocart').show();
        }) 
    });
   </script>
    
</body>
</html>