<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/headerPostlogin.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">  
     <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Praveen Guptha N</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>My Profile</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody">    
            <!-- container -->       
           <div class="container">

            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                <div class="col-md-4 col-sm-4">
                    <?php include 'includes/usrleftnav.php' ?>
                </div>
                <!--/ left col -->
                <!-- right col -->
                <div class="col-md-8 col-sm-8">
                    <!-- right profile detail -->
                    <div class="user-profile-rt">
                            <!-- row -->
                            <div class="row justify-content-center">
                                <!-- col -->
                                <div class="col-lg-6">  
                                    <!-- form -->
                                    <form class="form py-4">
                                        <!-- form group -->
                                        <div class="form-group">
                                            <label for="userName">Write Your Name</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="userName" aria-describedby="emailHelp" placeholder="Enter email" value="Praveen Guptha Nandipati">
                                            </div>
                                        </div>
                                        <!-- /form group -->

                                        <!-- form group -->
                                        <div class="form-group">
                                            <label for="userName">Gender Male / Female</label>
                                            <div class="d-flex">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="male" value="option1" checked>
                                                    <label class="form-check-label" for="male">
                                                        Male
                                                    </label>
                                                </div>
                                                <div class="form-check ml-4 form-check-inline">
                                                    <input class="form-check-input" type="radio" name="exampleRadios" id="female" value="option2">
                                                    <label class="form-check-label" for="female">
                                                    Female
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /form group -->

                                        <!-- form group -->
                                        <div class="form-group">
                                            <label for="mNumber">Mobile Number</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="mNumber" aria-describedby="emailHelp" placeholder="Enter Phone Number" value="91 9642123254">
                                            </div>
                                        </div>
                                        <!-- /form group -->

                                        <!-- form group -->
                                        <div class="form-group">
                                            <label for="email">Mobile Number</label>
                                            <div class="input-group">
                                                <input type="text" class="form-control" id="email" aria-describedby="emailHelp" placeholder="Enter Email Address" value="praveennandipati@gmail.com">
                                            </div>
                                        </div>
                                        <!-- /form group -->

                                        <!-- form group -->
                                        <div class="form-group">
                                        <button class="btn  orange-btn w-100">Submit</button>
                                        </div>
                                        <!-- /form group -->
                                    
                                    </form>
                                    <!--/ form -->
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ row -->

                        </div>
                        <!--/ right profile detail -->
                    </div>
                    <!--/ right col -->
                </div>
                <!--/row -->
                
            </div>
            <!--/ container -->                
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>   
    
</body>
</html>