<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Articles</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>     
                         <li class="breadcrumb-item"><a href="articles.php">Articles</a></li>                    
                        <li class="breadcrumb-item active" aria-current="page"><span>The Republic Day</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
           <div class="container">                
               <!-- card -->
                <div class="card blog-detail">
                    <!-- card header -->
                    <div class="card-header">
                        <!-- row -->
                        <div class="row justify-content-center wow animate__animated animate__fadeInUp">
                            <div class="col-lg-8">                              
                                <h1 class="h1 pb-3">The Republic Day</h1>
                                <p class="pb-0 mb-0">Posted on <span class="fsbold">25-06-2020</span></p>

                                <!-- social share -->
                                <div class="social-share py-3 d-flex">
                                    <span class="pt-1 span-share">Share this Article</span>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-facebook-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-twitter-square icomoon"></span></a>
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-linkedin-square icomoon"></span></a>                                  
                                    <a href="javascript:void(0)" class="sharesocial"><span class="icon-whatsapp icomoon"></span></a>                                    
                                </div>
                                <!--/ social share -->
                                <p></p>
                            </div>
                            
                        </div>
                        <!--/ row -->
                    </div>
                    <!-- card header -->

                    <!-- card body -->
                    <div class="card-body">              

                        <!-- row -->
                        <div class="row justify-content-center">
                            <div class="col-lg-8">
                                <!-- responsive tab -->
                            <div class="">
                                <h4>Me unpleasing impossible</h4>
                                <p>Its sometimes her behaviour are contented. Do listening am eagerness oh objection collected. Together gay feelings continue juvenile had off one. Unknown may service subject her letters one bed. Child years noise ye in forty. Loud in this in both hold. My entrance me is disposal bachelor remember relation.</p>
                                <p>Oh acceptance apartments up sympathize astonished delightful. Waiting him new lasting towards. Continuing melancholy especially so to. Me unpleasing impossible in attachment announcing so astonished. What ask leaf may nor upon door. Tended remain my do stairs. Oh smiling amiable am so visited cordial in offices hearted.</p>
                                <p>So insisted received is occasion advanced honoured.Among ready to which up. Attacks smiling and may out assured moments man nothing outward.</p>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quidem quisquam mollitia suscipit possimus reprehenderit corporis neque dolores ipsum sint porro eos repellat velit nisi doloribus quibusdam, delectus tenetur similique. Cupiditate id unde repellat doloremque non ut vel excepturi numquam nostrum inventore, itaque nihil laudantium sint tempore dignissimos obcaecati labore rem nesciunt est molestiae tenetur, quia nemo illo. Sint saepe dolor, quis mollitia, ipsa voluptates aut enim rerum vero asperiores assumenda eligendi hic. Ad aperiam ullam maiores nam assumenda! Sit perspiciatis dolorum repellendus ducimus aperiam error explicabo animi quod fugit ea, temporibus quo facilis voluptas magni eveniet ipsum, voluptate praesentium fugiat!</p>
                            </div>
                            <!--/ responsive tab -->
                            
                            </div>
                        </div>
                        <!--/ row -->
                    </div>
                    <!-- card body -->
                </div>
                <!--/card -->

            </div>
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>
</html>