<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Video Gallery</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page"><span>Video Gallery</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
            <!-- container -->
           <div class="container gallery-block">
                <!-- row -->
                <div class="row py-5">
                 <!-- col -->
                 <?php 
                    for($i=0;$i<count($videoAlbums);$i++){ ?>
                    <div class="col-sm-6 col-md-4 text-center video-col wow animate__animated animate__fadeInDown">
                        <iframe width="100%"  src="<?php echo $videoAlbums[$i][0] ?>" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        <p class="text-left"><?php echo $videoAlbums[$i][1] ?> </p>
                    </div>
                <?php } ?>
                <!--/ col -->

                </div>
                <!--/ row -->
                <!-- row -->
                <div class="row pb-5">
                    <div class="col-md-12">
                        <nav>
                            <ul class="pagination justify-content-center">                                        
                                <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                                    <span class="page-link" aria-hidden="true">‹</span>
                                </li>      
                                <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)" rel="next" aria-label="Next »">›</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
</body>
</html>