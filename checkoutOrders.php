<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Order Summary</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Order Summary</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody">
           
            <!--container -->
            <div class="container">
                <!-- row -->
                <div class="row py-3">
                    <!-- left col -->
                    <div class="col-lg-8">     

                    <p class="small fblue text-right py-3">Deliver by 24 August 2021</p>   

                        <!-- order summary -->
                        <div class="row checkout-row">
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <img src="img/books/1.jpg" alt="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-8 col-8">
                                <h6 class="h6 fsbold pb-2">Jayanthi Telugu 325 Pages Book</h6>
                                <p class="price-single forange pb-0">258</p>
                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2">
                                <p>
                                    <a class="pt-3 removeitem_eheckoutbtn" href="javascript:void(0)">Remove</a>
                                </p>
                            </div>
                            <!--/col -->
                        </div>  
                        <!--/ order summary --> 

                        <!-- order summary -->
                        <div class="row checkout-row">
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <img src="img/books/2.jpg" alt="" class="img-fluid">
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-8 col-8">
                                <h6 class="h6 fsbold pb-2">Jayanthi Telugu 325 Pages Book</h6>
                                <p class="price-single forange pb-0">258</p>
                                
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2">
                                <p>
                                    <a class="pt-3 removeitem_eheckoutbtn" href="javascript:void(0)">Remove</a>
                                </p>
                            </div>
                            <!--/col -->
                        </div>  
                        <!--/ order summary --> 
                        <p class="pt-3">
                            <a href="checkoutAddress.php" class="text-center"><span class="icon-arrows"></span> Change Address</a>
                         </p>
                    </div>
                    <!--/ left col -->

                     <!-- left col -->
                     <div class="col-lg-4 pt-4 pt-md-0">
                         <!-- card -->
                         <div class="card cartcard">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Order Summary</h4>
                             </div>
                             <div class="card-body">
                                 <p class="h6 d-flex justify-content-between  pb-4">
                                    <span>ITEMS 3</span>
                                    <span class="price-single">750</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between  pb-4 border-bottom">
                                    <span>Shipping Charges</span>
                                    <span class="price-single">150</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between fsbold py-4">
                                    <span>Total Cost</span>
                                    <span class="price-single fblue">900</span>
                                 </p> 
                                  <button onclick="window.location.href='paymentSuccess.php'" class="btn orange-btn ml-auto w-100" type="button" title="Send">Pay &nbsp;  <span class="price-single">900</span> </button>                               
                             </div>
                             
                         </div>
                         <!--/ card -->
                     </div>
                    <!--/ left col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

            
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>   
    
</body>
</html>