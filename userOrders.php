<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/headerPostlogin.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">  
     <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Praveen Guptha N</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                         <li class="breadcrumb-item"><a href="userProfile.php">My Profile</a></li>
                         <li class="breadcrumb-item active" aria-current="page"><span>My Orders</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody">    
            <!-- container -->       
           <div class="container">

            <!-- row -->
            <div class="row py-3 userprofile-row">
                <!-- left col -->
                <div class="col-md-4 col-sm-4">
                    <?php include 'includes/usrleftnav.php' ?>
                </div>
                <!--/ left col -->
                <!-- right col -->
                <div class="col-md-8 col-sm-8">

                <!-- right profile detail -->
                    <div class="user-profile-rt">                 

                        <!-- orders list item -->
                        <div class="myorder-list-item ">

                            <!-- basic details row -->
                            <div class="basic-details row">
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Ref No:</h5>
                                    <p>41-1592809666</p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-12 basic-details-col">
                                    <h5 class="h5">Order Date & Time</h5>
                                    <p>22-06-2019 at 2PM</p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Price</h5>
                                    <p class="forange">Rs:650</p>
                                </div>
                                <!--/ col -->
                               
                            </div>
                            <!--/ basic details row -->

                            <!-- secondary details row -->
                            <div class="row pt-3">
                                <!-- col -->
                                <div class="col-md-2">
                                    <a href="publication-detail.php">
                                        <img src="img/books/1.jpg" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-7">
                                    <p class="fsbold">
                                        <a href="publication-detail.php">Viswanatha Sahithyam Telugu Book</a>
                                    </p>
                                    <p>Qty:1</p>
                                    <p>Delivered Status: <span class="forange">Delivered on 12-06-2019</span></p>
                                    <p>Payment Status: <span class="text-success">Success</span></p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-3">
                                    <a href="javascript:void(0)"><span class="icon-cloud-download"></span> Get Invoice</a>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ secondary details row -->
                        </div>
                        <!--/ orders list item -->

                        
                        <!-- orders list item -->
                        <div class="myorder-list-item wow animate__animated animate__fadeInUp">

                            <!-- basic details row -->
                            <div class="basic-details row">
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Ref No:</h5>
                                    <p>41-1592809666</p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Date & Time</h5>
                                    <p>22-06-2019 at 2PM</p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Price</h5>
                                    <p class="forange">Rs:650</p>
                                </div>
                                <!--/ col -->                               
                            </div>
                            <!--/ basic details row -->

                            <!-- secondary details row -->
                            <div class="row pt-3">
                                <!-- col -->
                                <div class="col-md-2">
                                    <a href="publication-detail.php">
                                        <img src="img/books/2.jpg" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-7">
                                    <p class="fsbold">
                                        <a href="publication-detail.php">Viswanatha Sahithyam Telugu Book</a>
                                    </p>
                                    <p>Qty:1</p>
                                    <p>Delivered Status: <span class="text-primary">Delivery Process</span></p>
                                    <p>Payment Status: <span class="text-danger">Cancelled</span></p>
                                </div>
                                <!--/ col -->
                                
                            </div>
                            <!--/ secondary details row -->
                        </div>
                        <!--/ orders list item -->

                        
                        <!-- orders list item -->
                        <div class="myorder-list-item wow animate__animated animate__fadeInDown">

                            <!-- basic details row -->
                            <div class="basic-details row">
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Ref No:</h5>
                                    <p>41-1592809666</p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Date & Time</h5>
                                    <p>22-06-2019 at 2PM</p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-6 col-lg-4 col-6 basic-details-col">
                                    <h5 class="h5">Order Price</h5>
                                    <p class="forange">Rs:650</p>
                                </div>
                                <!--/ col -->                               
                            </div>
                            <!--/ basic details row -->

                            <!-- secondary details row -->
                            <div class="row pt-3">
                                <!-- col -->
                                <div class="col-md-2">
                                    <a href="publication-detail.php">
                                        <img src="img/books/3.jpg" alt="" class="img-fluid">
                                    </a>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-7">
                                    <p class="fsbold">
                                        <a href="publication-detail.php">Viswanatha Sahithyam Telugu Book</a>
                                    </p>
                                    <p>Qty:1</p>
                                    <p>Delivered Status: <span class="forange">Delivery Process</span></p>
                                    <p>Payment Status: <span class="text-success">Success</span></p>
                                </div>
                                <!--/ col -->
                                <!-- col -->
                                <div class="col-md-3">
                                    <a href="javascript:void(0)"><span class="icon-cloud-download"></span> Get Invoice</a>
                                </div>
                                <!--/ col -->
                            </div>
                            <!--/ secondary details row -->
                        </div>
                        <!--/ orders list item -->

                    </div>
                    <!--/ right profile detail -->
                   
                </div>
                <!--/ right col -->
            </div>
            <!--/row -->
                
            </div>
            <!--/ container -->                
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>     
    
</body>
</html>