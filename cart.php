<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Cart Items</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Cart Items</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody">
           
            <!--container -->
            <div class="container">
                <!-- row -->
                <div class="row py-3">
                    <!-- left col -->
                    <div class="col-lg-8 wow animate__animated animate__fadeInUp">
                        <!-- row -->
                        <div class="table-headerrow row">
                            <!-- col -->
                            <div class="col-md-6">
                               <p>Product Details</p>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2">
                                <p>Qty</p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2">
                                <p>Price</p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2">
                                <p>Total</p>
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row cartlist-itemrow">
                            <!-- col -->
                            <div class="col-md-6 col-12">
                               <div class="d-flex">
                                    <a href="publication-detail.php" class="d-block cartimteimg">
                                        <img src="img/books/1.jpg" alt="" class="w-100">
                                    </a>
                                    <div class="cart-item-details">
                                        <h6 class="h6 pb-2 fsbold">
                                            <a href="publication-detail" class="fgray">Book name will be here</a>
                                        </h6>
                                        <p class="pb-0 lgray"> Language: <span class="fbold">English</span></p>
                                        <p class="lgray"> Product Code: <span class="fbold">VP001</span></p>

                                        <p>
                                            <a href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                               </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <p>
                                    <input type="number" class="form-control" value="1">
                                </p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single"> 250  </p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single fsbold forange"> 250  </p>
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row cartlist-itemrow">
                            <!-- col -->
                            <div class="col-md-6 col-12">
                               <div class="d-flex">
                                    <a href="publication-detail.php" class="d-block cartimteimg">
                                        <img src="img/books/2.jpg" alt="" class="w-100">
                                    </a>    
                                    <div class="cart-item-details">
                                        <h6 class="h6 pb-2 fsbold">
                                            <a href="publication-detail" class="fgray">Book name will be here</a>
                                        </h6>
                                        <p class="pb-0 lgray"> Language: <span class="fbold">English</span></p>
                                        <p class="lgray"> Product Code: <span class="fbold">VP001</span></p>
                                        <p>
                                            <a href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                               </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <p>
                                    <input type="number" class="form-control" value="1">
                                </p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single"> 250  </p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single fsbold forange"> 250  </p>
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        
                        <!-- row -->
                        <div class="row cartlist-itemrow">
                            <!-- col -->
                            <div class="col-md-6 col-12">
                               <div class="d-flex">
                                    <a href="publication-detail.php" class="d-block cartimteimg">
                                        <img src="img/books/3.jpg" alt="" class="w-100">
                                    </a>
                                    <div class="cart-item-details">
                                        <h6 class="h6 pb-2 fsbold">
                                            <a href="publication-detail" class="fgray">Book name will be here</a>
                                        </h6>
                                        <p class="pb-0 lgray"> Language: <span class="fbold">English</span></p>
                                        <p class="lgray"> Product Code: <span class="fbold">VP001</span></p>
                                        <p>
                                            <a href="javascript:void(0)">Remove</a>
                                        </p>
                                    </div>
                               </div>
                            </div>
                            <!--/ col -->
                            <!-- col -->
                            <div class="col-md-2 col-4">
                                <p>
                                    <input type="number" class="form-control" value="1">
                                </p>
                            </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single"> 150  </p>
                             </div>
                            <!--/ col -->
                             <!-- col -->
                             <div class="col-md-2 col-4">
                                <p class="price-single fsbold forange"> 150  </p>
                             </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                            <p class="pt-3 text-right">
                                <a href="checkoutAddress.php" class="orange-btn text-center w-100">Place Order</a>
                            </p>

                    </div>
                    <!--/ left col -->

                     <!-- left col -->
                     <div class="col-lg-4 wow animate__animated animate__fadeInDown">
                         <!-- card -->
                         <div class="card cartcard">
                             <div class="card-header bggray">
                                 <h4 class="h6 fsbold">Order Summary</h4>
                             </div>
                             <div class="card-body">
                                 <p class="h6 d-flex justify-content-between  pb-4">
                                    <span>ITEMS 3</span>
                                    <span class="price-single">750</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between  pb-4 border-bottom">
                                    <span>Shipping Charges</span>
                                    <span class="price-single">150</span>
                                 </p>
                                 <p class="h6 d-flex justify-content-between fsbold py-4">
                                    <span>Total Cost</span>
                                    <span class="price-single fblue">900</span>
                                 </p>
                                
                             </div>
                         </div>
                         <!--/ card -->
                     </div>
                    <!--/ left col -->
                </div>
                <!--/ row -->
            </div>
            <!--/ container -->

            
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>
</html>