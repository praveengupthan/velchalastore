<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Audio Gallery</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page"><span>Audio Gallery</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
           <div class="container">                
                <!-- row -->
                <div class="row pt-3">
                    <!-- col -->
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                               <thead class="thead-dark">
                                    <tr>
                                        <th>Name of Track</th>
                                        <th>Play</th>
                                    </tr>
                               </thead>
                               <tbody>
                                    <tr>
                                        <td>Achutam Keshavam</td>
                                        <td>
                                            <a href="https://soundcloud.com/nandipati-praveen/achutam-keshavam-krishna-darshan-to-denge-kabhi-na-kabhi-krishna-bhajan-krishnasong" target="_blank">Play Now</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name of Track</td>
                                        <td>
                                            <a href="https://soundcloud.com/nandipati-praveen/achutam-keshavam-krishna-darshan-to-denge-kabhi-na-kabhi-krishna-bhajan-krishnasong" target="_blank">Play Now</a>
                                        </td>
                                    </tr>
                               </tbody>
                            </table>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/row -->

            </div>
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>
</html>