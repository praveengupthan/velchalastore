<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Event Name will be here</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>
                        <li class="breadcrumb-item"><a href="photoAlbums.php">Photos</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><span>Event Name</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
            <!-- container -->
           <div class="container gallery-block grid-gallery">
                <!-- row -->
                <div class="row">
                    <!-- item -->
                    <?php 
                    for($i=0;$i<count($galleryPhotoDetail);$i++) {?>
                    <div class="col-6 col-sm-6 col-md-3 item wow animate__animated animate__fadeInUp">
                        <a class="lightbox" href="img/albums/<?php echo $galleryPhotoDetail[$i][0]?>.jpg">
                            <img class="img-fluid image scale-on-hover" src="img/albums/<?php echo $galleryPhotoDetail[$i][0]?>.jpg">
                        </a>
                    </div>
                    <?php } ?>
                    <!-- item -->                   
               </div>
               <!--/ row -->
           </div>
           <!--/ container -->
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    <script>
        baguetteBox.run('.grid-gallery', { animation: 'slideIn'});
    </script>
</body>
</html>