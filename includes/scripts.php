<!-- scripts -->
    <script src="node_modules/jquery/dist/jquery.min.js"></script>
    <script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="node_modules/popper.js/dist/esm/popper.min.js"></script>
    <script src="node_modules/swiper/swiper-bundle.min.js"></script>
    <script src="node_modules/easy-responsive-tabs/js/easyResponsiveTabs.js"></script>    
    <script src="node_modules/jquery-validation/dist/jquery.validate.js"></script>
    <script src="js/bsnav.min.js"></script>
    <!--[if lte IE 9]>
        <script src="node_modules/fix-ie/dist/ie.lteIE9.js"></script>
    <![endif]-->   

    <!-- script files for grid gallery -->
    <script src="js/baguetteBox.js"></script>  
    
    <script src="js/custom.js"></script>
    
      
    <script>
        //contact form validatin
        $('#contact_form').validate({
            ignore: [],
            errorClass: 'text-danger', // You can change the animation class for a different entrance animation - check animations page
            errorElement: 'div',
            errorPlacement: function (error, e) {
                e.parents('.form-group').append(error);
            },
            highlight: function (e) {
                $(e).closest('.form-group').removeClass('has-success has-error').addClass('has-error');
                $(e).closest('.text-danger').remove();
            },

            rules: {
                name: {
                    required: true
                },
                phone: {
                    required: true,
                    number: true,
                    minlength: 10,
                    maxlength: 10
                },
                email: {
                    required: true,
                    email: true
                },
                howDid: {
                    required: true
                },
                sub: {
                    required: true,
                }
            },

            messages: {
                name: {
                    required: "Enter Name"
                },
                phone: {
                    required: "Enter Valid Mobile Number"
                },
                email: {
                    required: "Enter Valid Email"
                },
                email: {
                    required: "How did you know abut us"
                },
                sub: {
                    required: "Enter Subject"
                }
            },
        });
    </script>
    
    <!--/ scripts -->