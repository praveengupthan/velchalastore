 <!-- header -->
    <header class="bsnav-sticky">
        <div class="navbar navbar-expand-lg bsnav">
            <a class="navbar-brand" href="books.php">
                <img src="img/logo.svg" alt="Velchala Kondal Rao">
            </a>
            <button class="navbar-toggler toggler-spring"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse justify-content-md-between">
                <!-- left nav -->
                <ul class="navbar-nav navbar-mobile mx-auto">    
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php.php'){echo'active';}else {echo'nav-link';}?>" href="index.php.php">Home</a></li>                                   
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='books.php'){echo'active';}else {echo'nav-link';}?>" href="books.php">Books</a></li>
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='jayanthimagazines.php'){echo'active';}else {echo'nav-link';}?>" href="jayanthimagazines.php">Jayanthi</a></li>
                    <li class="nav-item dropdown fadeup"><a class="<?php if(
                            basename($_SERVER[ 'SCRIPT_NAME'])=='poems.php' ||                           
                            basename($_SERVER[ 'SCRIPT_NAME'])=='photoAlbums.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='videoGallery.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='audioGallery.php'
                            ) {echo 'active'; } else {echo ''; }?> nav-link" href="#">Gallery <i class="caret"></i></a>
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'poems.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="poems.php">Poems</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'photoAlbums.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="photoAlbums.php">Photos</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'videoGallery.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="videoGallery.php">Videos</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'audioGallery.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="audioGallery.php">Audios</a></li>
                        </ul>
                    </li>
                     <li class="nav-item dropdown fadeup"><a class="<?php if(
                            basename($_SERVER[ 'SCRIPT_NAME'])=='articles.php' ||                           
                            basename($_SERVER[ 'SCRIPT_NAME'])=='news.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='interviews.php'||
                             basename($_SERVER[ 'SCRIPT_NAME'])=='generalEvents.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='jayanthiEvents.php'
                            ) {echo 'active'; } else {echo ''; }?> nav-link" href="#">Media <i class="caret"></i></a>
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'articles.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="articles.php">Articles</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'news.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="news.php">News</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'interviews.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="interviews.php">Interviews</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'generalEvents.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="generalEvents.php">General Events</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'jayanthiEvents.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="jayanthiEvents.php">Jayanthi Events</a></li>
                        </ul>
                    </li>                    
                    <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'active';}else {echo'nav-link';}?>" href="contact.php">Contact</a></li>                   
                </ul>
                <!-- left nav-->

                <!-- right nav -->
                <!-- left nav -->
                <ul class="navbar-nav navbar-mobile mr-0 ">  
                    <li class="nav-item">
                        <a class="nav-link cart-link" href="cart.php">
                            <span class="icon-shopping-cart1 icomoon"></span>
                            <span class="value">10</span>
                        </a>
                    </li>
                      <li class="nav-item dropdown fadeup"><a class="<?php if(
                            basename($_SERVER[ 'SCRIPT_NAME'])=='userProfile.php' ||                           
                            basename($_SERVER[ 'SCRIPT_NAME'])=='userOrders.php'||
                            basename($_SERVER[ 'SCRIPT_NAME'])=='userChangePassword.php'                            
                            ) {echo 'active'; } else {echo ''; }?> nav-link" href="#"><span class="icon-user"></span>Praveen Kumar <i class="caret"></i></a>
                        <ul class="navbar-nav">
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'userProfile.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="userProfile.php">My Profile</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'userOrders.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="userOrders.php">My Orders</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'userChangePassword.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="userChangePassword.php">Change Password</a></li>
                            <li class="nav-item"><a class="<?php if(basename($_SERVER['SCRIPT_NAME']) == 'books.php'){echo 'activesubnav'; }else { echo 'nav-link'; } ?>" href="books.php">Logout</a></li>                            
                        </ul>
                    </li> 
                </ul>
                <!-- left nav-->
                <!--/ right nav -->                
            </div>
        </div>
        <div class="bsnav-mobile">
            <div class="bsnav-mobile-overlay"></div>
            <div class="navbar"></div>
        </div>
    </header>
    <!--/ header -->