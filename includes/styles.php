<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">
<link rel="stylesheet" href="node_modules/bootstrap/dist/css/bootstrap.min.css"> 
<link rel="stylesheet" href="node_modules/swiper/swiper-bundle.min.css">    
<link rel="stylesheet" href="node_modules/easy-responsive-tabs/css/easy-responsive-tabs.css">
<link rel="stylesheet" href="css/grid-gallery.css">
<link rel="stylesheet" href="css/baguetteBox.css">
<link rel="stylesheet" href="css/icomoon.css">
<link rel="stylesheet" href="css/bsnav.min.css">
<link rel="stylesheet" href="css/home.css">
<link rel="stylesheet" href="css/style.css">
