<?php     //array objects of publcations list 
    $publications=array(
        array(
            "1",
            "Biqree Shabnam"           
        ),    
        array(
            "2",
            "Telangana Tejam"           
        ), 
        array(
            "3",
            "Poems To Note Quote And Float"           
        ), 
        array(
            "4",
            "Gleanings And Rambling Thoughts"           
        ), 
        array(
            "5",
            "Seshendra A Poetic Legend"           
        ), 
        array(
            "6",
            "Essence Of The Essential"           
        ), 
        array(
            "7",
            "18 Greats Of India"           
        ), 
        array(
            "8",
            "Short Stories Of Viswanadha"           
        ), 
        array(
            "9",
            "Geetanjali"           
        ), 
        array(
            "10",
            "Mahanati Savitri"           
        ), 
        array(
            "11",
            "Thousand Hoods"           
        ), 
        array(
            "12",
            "Poetry Pattabi"           
        ),     
    );

    //array objects of jayanthi

    $jayanthiCover=array(
        array("1", "January 2020"),
        array("2", "February 2020"),
        array("3", "March 2020"),
        array("4", "April 2020"),
        array("5", "May 2020"),
        array("6", "June 2020"),
        array("7", "July 2020"),
        array("8", "August 2020"),
        array("9", "September 2020"),
        array("10", "October 2020"),
        array("11", "November 2020"),
        array("12", "December 2020"),
    );

    //array objects poems
    $galleryPoems=array(
        array("1"),
        array("2"),
        array("3"),
        array("4"),
        array("5"),
        array("6"),
        array("7"),
        array("8"),
        array("9"),
        array("10")       
    );

    //array photo albums 
    $photoAlbums=array(
        array("1","50 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("2","20 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("3","30 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("4","40 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("5","50 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("6","10 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("7","20 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("8","30 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("9","40 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("1","50 Photos","Event Name will be here","Hyderabad","22-11-2019"),
        array("2","60 Photos","Event Name will be here","Hyderabad","22-11-2019"),
    );

    //array photo album detail
    $galleryPhotoDetail=array(
        array("1"),
        array("2"),
        array("3"),
        array("4"),
        array("5"),
        array("6"),
        array("7"),
        array("8"),
        array("9"),
        array("1"),
        array("2"),
        array("3"),
        array("4"),
        array("5"),
        array("6"),
        array("7"),
        array("8"),
        array("9")       
    );
    //array video gallery 
    $videoAlbums=array(
        array("https://www.youtube.com/embed/VNtVaV7U0BU","HMTV Special Focus On Sahitya Sangamam Rao",),
        array("https://www.youtube.com/embed/lLhadiHjiRE","HMTV Special Focus On Sahitya Sangamam  Part",),
        array("https://www.youtube.com/embed/5hpszhcgEyQ","HMTV Special Focus On Sahitya Sangamam Part",),
        array("https://www.youtube.com/embed/VNtVaV7U0BU","HMTV Special Focus On Sahitya Sangamam Rao",),
        array("https://www.youtube.com/embed/lLhadiHjiRE","HMTV Special Focus On Sahitya Sangamam  Part",),
        array("https://www.youtube.com/embed/5hpszhcgEyQ","HMTV Special Focus On Sahitya Sangamam Part",),
        array("https://www.youtube.com/embed/VNtVaV7U0BU","HMTV Special Focus On Sahitya Sangamam Rao",),
        array("https://www.youtube.com/embed/lLhadiHjiRE","HMTV Special Focus On Sahitya Sangamam  Part",),
        array("https://www.youtube.com/embed/5hpszhcgEyQ","HMTV Special Focus On Sahitya Sangamam Part",),
    );

    //array blog articles 
    $blogArticles=array(
        array(
            "The Republic Day",
            "Yet another year comes – A day, so called, a new day. It passes with a name. In a few moments of glitter and glee It mingles with the stillness, the shrillness of time",         
        ),
        array(
            "Rose",
            "Rose!
            You are a charm incomparable!
            I wonder, whether you are a flower
            Or a panorama of colours,
            A pageantry of smiles?",         
        ),       
    );

    //blog event array objects
    $blogEventItem=array(
        array(
            "1",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "25-06-2020"            
        ),
        array(
            "2",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Warangal",
            "21-08-2020"            
        ),
        array(
            "3",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "10-02-2020"            
        ),
        array(
            "4",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "15-04-2020"            
        ),
        array(
            "5",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "06-07-2020"            
        ),
        array(
            "6",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "16-06-2020"            
        ),
        array(
            "7",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Warangal",
            "25-06-2020"            
        ),
        array(
            "8",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Secunderabd",
            "25-06-2020"            
        ),
        array(
            "9",
            "Event Title will be here",
            "Some quick example text to build on the card title and make up the bulk of the card's content.",
            "Hyderabad",
            "25-06-2020"            
        )
    );


//books
$bookSlider = array (
    array("1"),
    array("2"),
    array("3"),
    array("4"),    
    array("6"),
    array("7"),
    array("8"),
    array("9"),
    array("10"),
    array("11"),
    array("12"),
    array("13"),
    array("14"),
    array("15"),
    array("16"),
    array("17"),
    array("18"),
    array("19"),
    array("20"),
    array("21"),
    array("22"),
    array("23"),
    array("24"),
    array("25"),
    array("26"),
    array("27"),
    array("28"),
    array("29"),
    array("30"),
    array("31"),
    array("32"),
    array("33"),
    array("34"),
    array("35"),
    array("36"),
    array("37"),
    array("38"),
    array("39"),
    array("40"),    
);

?>