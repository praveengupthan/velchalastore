<!-- user left nav -->
<div class="user-leftNav whitebox">   
    <ul>
        <li>
            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userProfile.php'){echo'activeuserNav';}else {echo'userNavLink';}?>" href="userProfile.php">Profile</a>
        </li>
        <li>
            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userAddressManager.php'){echo'activeuserNav';}else {echo'userNavLink';}?>" href="userAddressManager.php">Manage Address</a>
        </li>
        <li>
            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userOrders.php'){echo'activeuserNav';}else {echo'userNavLink';}?>" href="userOrders.php">Orders History</a>
        </li>       
        <li>
            <a class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='userChangePassword.php'){echo'activeuserNav';}else {echo'userNavLink';}?>" href="userChangePassword.php">Change Password</a>
        </li>
        <li>
            <a class="userNavLink" href="books.php">Logout</a>
        </li>
    </ul>
</div>
<!--/ user left nav -->