<!-- footer -->
    <footer>
        <!-- container -->
        <div class="container">
            <!-- nav -->
            <ul class="nav justify-content-center aos-item" >
                <li class="nav-item">
                    <a href="index.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Home</a>
                </li>

                 <li class="nav-item">
                    <a href="index.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='index.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Velchala</a>
                </li>

                <li class="nav-item">
                    <a href="books.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='books.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Books</a>
                </li>
                
                <li class="nav-item">
                    <a href="jayanthimagazines.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='jayanthimagazines.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Jayanthi</a>
                </li>
                <li class="nav-item">
                    <a href="contact.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='contact.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="terms.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='terms.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Terms & conditions</a>
                </li>
                <li class="nav-item">
                    <a href="privacy.php" class="<?php if(basename($_SERVER['SCRIPT_NAME'])=='privacy.php'){echo'activeFooterNav';}else {echo'nav-link';}?>">Privacy Policy</a>
                </li>                
               
            </ul>
            <!--/ nav -->
           
            <ul class="nav justify-content-center socialnav">
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-facebook icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-twitter icomoon"></span></a>
                </li>
                <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-linkedin icomoon"></span></a>
                </li>
                 <li class="nav-item">
                    <a href="javascript:void(0)"  class="nav-link"><span class="icon-youtube-play icomoon"></span></a>
                </li>
            </ul>  
             <p class="text-center"><i>© 2021 Velchala Kondal Rao. All rights reserved. </i></p>                     
        </div>
        <!--/container -->
        <a id="movetop" href="#" class="movetop"><span class="icon-arrow-up icomoon"></span></a>
    </footer>
    <!--/ footer -->