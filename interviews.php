<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>Interviews</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page"><span>Interviews</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
           <div class="container">                
             <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <div class=" col-sm-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="card ">
                            <iframe width="100%" height="225" src="https://www.youtube.com/embed/sXOQ0l3aRzQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">                       
                                <p class="card-text pb-3">Kondal Rao Interview on his Personal Life</p>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInDown">
                        <div class="card ">
                            <iframe width="100%" height="225" src="https://www.youtube.com/embed/xRr-Nn73UHs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">                       
                                <p class="card-text pb-3">Dr VELCHALA KONDAL RAO GARU BOOK RELEASE....</p>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="card">
                            <iframe width="100%" height="225" src="https://www.youtube.com/embed/dWFvZl3D3us" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            <div class="card-body">                       
                                <p class="card-text pb-3">Dr. Velchala Kondal Rao Speech on Viswanadha Sahitya Peetham ....</p>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row pb-5">
                    <div class="col-md-12">
                        <nav>
                            <ul class="pagination justify-content-center">                                        
                                <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                                    <span class="page-link" aria-hidden="true">‹</span>
                                </li>      
                                <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)" rel="next" aria-label="Next »">›</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--/ row -->

            </div>
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>
</html>