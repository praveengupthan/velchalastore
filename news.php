<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>
   <?php 
   include 'includes/header.php';
   include 'includes/arrayObjects.php'   
   ?>
    <!-- page -->
    <div class="subPage">
        <!-- subpage Header -->
        <div class="subPageHeader">
            <!-- container -->
            <div class="container">
                <h1>News</h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="books.php">Books</a></li>                        
                        <li class="breadcrumb-item active" aria-current="page"><span>News</span></li>
                    </ol>
                </nav>
            </div>
            <!--/ container -->
        </div>
        <!--/ sub page header -->

        <!-- sub page body -->
        <div class="subPageBody"> 
           <div class="container">                
              <!-- row -->
                <div class="row py-3">
                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInDown">
                        <div class="card blogcard">
                            <a href="https://www.thehansindia.com/posts/index/Opinion/2013-07-26/Velchala-Kondal-Rao-An-eminent-educationist/66968?infinitescroll=1" target="_blank">
                                <img class="card-img-top img-fluid" src="img/gallery/gal01.JPG">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title ptregular">An eminent educationist</h5>
                                <p class="card-text pb-3">The 80th birthday of Velchala Kondal Rao falls on July 21 All this changed after Kondal Rao took charge. ...</p>
                                <p>The Hindu India <span class="d-inline-block px-3 small pb-3">|</span>26-07-2013</p>
                                <a href="https://www.thehansindia.com/posts/index/Opinion/2013-07-26/Velchala-Kondal-Rao-An-eminent-educationist/66968?infinitescroll=1" class="btn orange-btn" target="_blank">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInUp">
                        <div class="card blogcard">
                            <a href="https://www.fourth.in/v-kondal-rao-commerce-lecturer/" target="_blank">
                                <img class="card-img-top img-fluid" src="img/gallery/news01.jpg">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title ptregular">A treasure of talent  ....</h5>
                                <p class="card-text pb-3">At the age of 87, not everyone is blessed to be Hale and hearty. But here’s an octogenarian  ...</p>
                                <p>Fourth <span class="d-inline-block px-3 small pb-3">|</span>17-08-2019</p>
                                <a href="https://www.fourth.in/v-kondal-rao-commerce-lecturer/" class="btn orange-btn" target="_blank">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-sm-6 col-lg-4 wow animate__animated animate__fadeInDown">
                        <div class="card blogcard">
                            <a href="https://www.hybiz.tv/velchala-kondal-rao-president-tescls-welcome-on-behalf-of-tescls/velchala-kondal-rao-president-tescls-welcome-on-behalf-of-tescls-2/" target="_blank">
                                <img class="card-img-top img-fluid" src="img/gallery/news01.jpg">
                            </a>
                            <div class="card-body">
                                <h5 class="card-title ptregular">President TESCLS</h5>
                                <p class="card-text pb-3">Velchala Kondal Rao President TESCLS | Welcome On Behalf Of TESCLS</p>
                                <p>Hybiz TV <span class="d-inline-block px-3 small pb-3">|</span>NA</p>
                                <a href="https://www.hybiz.tv/velchala-kondal-rao-president-tescls-welcome-on-behalf-of-tescls/velchala-kondal-rao-president-tescls-welcome-on-behalf-of-tescls-2/" class="btn orange-btn" target="_blank">Read More</a>
                            </div>
                        </div>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row -->

                <!-- row -->
                <div class="row pb-5">
                    <div class="col-md-12">
                        <nav>
                            <ul class="pagination justify-content-center">                                        
                                <li class="page-item disabled" aria-disabled="true" aria-label="« Previous">
                                    <span class="page-link" aria-hidden="true">‹</span>
                                </li>      
                                <li class="page-item active" aria-current="page"><span class="page-link">1</span></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">2</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)">3</a></li>
                                <li class="page-item"><a class="page-link" href="javascript:void(0)" rel="next" aria-label="Next »">›</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <!--/ row -->

            </div>
        </div>
        <!--/ sub page body -->
    </div>
    <!--/ page -->   
    <?php include 'includes/footer.php'?>
    <?php include 'includes/scripts.php'?>
    
</body>
</html>