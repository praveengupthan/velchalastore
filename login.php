<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Velchala Kondal Rao</title>
    <?php include 'includes/styles.php'?>
</head>
<body>  

<div class="login-page">
        <!-- container fluid -->
        <div class="container-fluid">
            <!-- row -->
            <div class="row justify-content-center">
                <!-- col -->
                <div class="col-md-6 align-self-center">
                    <!-- login section -->
                    <div class="login-section">
                        <div class="login-top">
                            <a href="books.php" class="brand-login">
                                <img src="img/logo.svg" alt="">
                            </a>
                            <h1 class="text-center flight pb-0">Login Account</h1>
                          
                        </div>
                        <!-- form -->
                        <form class="form py-3">
                            <div class="form-group">
                                <label for="userNameInput">Registered Mobile Number</label>
                                <div class="input-group">
                                    <input type="text" class="form-control" id="userNameInput" placeholder="Registered Mobile Number">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userNameInput">Enter your Password</label>
                                <div class="input-group">
                                    <input type="password" class="form-control" id="userNameInput" placeholder="Password">
                                </div>
                            </div>
                            <p class="text-right"><a href="forgotPassword.php" class="fblue">Forgot Password?</a></p>
                            <input type="button" onclick="pageRedirect()" class="btn orange-btn w-100 mt-2" value="Sign in">
                            <p class="text-center py-2">
                                Don't have an account? <a class="forange" href="register.php">Register now</a>
                            </p>
                            <script>
                                function pageRedirect() {
                                    window.location.href = "userProfile.php";
                                    }   
                                </script>
                        </form>
                        <!--/ form -->
                    </div>
                    <!--/ login section -->
                </div>
                <!--/ col -->                
            </div>
            <!--/row -->
        </div>
        <!--/ container fluid -->
    </div>   
   

    <?php include 'includes/scripts.php'?>

   
    
</body>
</html>